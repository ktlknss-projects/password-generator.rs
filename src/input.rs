use std::io;

pub fn prompt() -> String {
    let mut user_input = String::new();
    match io::stdin().read_line(&mut user_input) {
        Ok(_) => user_input,

        Err(why) => {
            eprintln!("Error reading input: {}", why);
            return String::new(); // Return an empty string on error
        }
    }
}
