mod input;

mod pw_generator;

mod clip_board;

use crate::{clip_board::add_to_clipboard, input::prompt, pw_generator::generate_password};

fn main() {
    println!("Welcome to Password Generator Rust");
    println!("How long of a password do you want to generate?");
    let length_string = prompt();

    let length = match length_string.trim().parse::<u8>() {
        Ok(num) => num,
        Err(why) => {
            println!("Failed to parse number: {}", why);
            0
        }
    };

    println!("What character set do you want to use? (find in char_sets, don't add .txt)");
    let char_set = prompt();

    let password = generate_password(length, char_set.as_str());

    println!("Here is your password: {}", password);

    println!("Do you want to copy it? (Y/N)");
    if prompt().to_lowercase() == "y" {
        add_to_clipboard(&password)
    }
}
