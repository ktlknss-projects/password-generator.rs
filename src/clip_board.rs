use clipboard::ClipboardContext;
use clipboard::ClipboardProvider;

pub fn add_to_clipboard(str: &str) {
    let mut clip_ctx: ClipboardContext = ClipboardProvider::new().unwrap();

    clip_ctx.set_contents(str.to_owned()).unwrap();
}
