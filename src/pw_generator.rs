use rand::Rng;

use std::fs::File;
use std::io::prelude::*;

pub fn generate_password(length: u8, char_set: &str) -> String {
    let matched_char_set = match char_set.trim().to_lowercase().as_str() {
        "" | "\n" | " " => "default",
        "number" | "numbers" | "pin" => "numbers",
        _ => char_set,
    };
    // Reads file
    let mut file =
        File::open(format!("char_sets/{}.txt", matched_char_set)).expect("File not found");
    let mut chars = String::new();
    file.read_to_string(&mut chars).expect("Cannot read file");

    let chars_length = chars.len();

    let mut password = String::new();

    for _i in 0..length {
        let index = rand::thread_rng().gen_range(0..chars_length);
        // let random_char = chars
        //     .char_indices()
        //     .nth(index)
        //     .expect("Error finding random number")
        //     .1;
        let random_char = chars
            .split("")
            .map(|split_chars| split_chars.to_string()[index]);
        password.push(random_char)
    }

    password
}
